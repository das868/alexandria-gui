# `alexandria-gui` – GUI version of the alexandria library management tool

## Preamble
This program under the MIT License is dedicated to all the binge-readers out there who lack a convenient tool to organize their private library but don't wanna dive too deep into SQL and relational databases. The simple yet serious goal of this project is to provide just that – and to prevent you from encountering those moments ever again, when you come home from your trusted bookstore and aren't sure if you've bought something you already have so that you gotta search through your entire flat. Tell me in the *Issues* section above whether your expecations were met by the software after reading this.

And it is dedicated to one particular member of this club of literature junkies, and I'm sure she knows I'm winking at her at this moment ;)

## List of contents
The following files are shipped within this project:
- main.py – containing all the actual functionality, assigning and wiring
- mainwindow.ui – Qt UI for the main window
- dialog.ui – Qt UI describing the dialog used to add and modify book entries
- dialog2-minimal.ui – Qt UI describing the lightweight dialog used to search the database for a specific book
- test.db – a sample database containing the table `Bücher` (="books"); mainly for testing purposes
- LICENSE – MIT License under which you can distribute and modify the project
- and this README file!

You can get them by running
    git clone https://gitlab.com/das868/alexandria-gui.git
from within your desired installation directory.

## Dependencies
You need to have an up-to-date version of Python3, `sqlite3` and Qt5 (in particular PyQt5) installed on your system for this to work.

## Features
`alexandria-gui` is still under development, but already features core functionality like:
- adding books to a table called `Bücher` within an `sqlite3` database
- printing the contents of that database to the terminal (linux only)
- fuzzy searching it for specific books containing given author, title and shelf position 
- changing all database entries, e.g. updating the shelf position or correcting a spelling mistake without having to drop the entire database or table
- deleting single database entries,
all of which are availible from the GUI!

## What's still missing
I know, it's far away from being perfect... Just take a look at this list (and write and Issue if you're still missing something!):
- full-feature database model with
	- unique ID (as PRIMARY KEY)
	- Autor
	- Titel
	- Untertitel
	- Verlag
	- Ejahr
	- Genre 
	- Sprache
	- Regkoord
	- Last_action (via `DATE DEFAULT (datetime('now', 'localtime'))` call with every modification of the entry)
- global exception handling
- OOP approach with own `MainWindow` class etc.
- sensible approach to all the bioler-plate code
- Windows and MacOS support
- cross-platform installation script
- multiple language support (default is currently German)


## Release notes on `v1-prealpha1` (2018-09-23)
Be warned! The UIs still look quite crappy, whereas all the functionality is implemented an ready to rock'n'roll! However, little to no testing was conducted outside a Manjaro-Linux Python3.7.0 and Qt5.11 (PyQt5 5.11.2) environment so that the is no guarantee for the software to work on your linux setup or even Windows or MacOS computer. But bare with me, I keep working on it! I can already smell the pending `v1-alpha1` version...

## Thanks to the developers of...
- Python 3.7.0
- Qt 5.11.1
- Qt Designer
- PyQt5 5.11.2
- Sqlite3 3.24.0
- [VIM](https://github.com/vim/vim "The one and only CLI editor!") 8.1.301, [Bram Moolenar](https://github.com/brammool "his GitHub account")
- Manjaro Linux

-----
But most importantly: Have fun with this! Go ahead and improve it to fit your needs, issue a pull request if you consider your adaptions helpful. 'Cause any help is always greatly appreciated!

-----
Keep on reading and check out my other projects when you need a break! 
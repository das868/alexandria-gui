#!/usr/bin/env python3

import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtGui as gui
import PyQt5.uic as uic
import sys
import sqlite3


app = widgets.QApplication(sys.argv)
mainwindow = uic.loadUi("mainwindow.ui")
dialog = uic.loadUi("dialog.ui")
dialog2 = uic.loadUi("dialog2-minimal.ui") # old: dialog2 = uic.loadUi("dialog2.ui")
print("setup done")

infolist = list()
db_conn = sqlite3.connect("test.db")
cursor = db_conn.cursor()
print("infolist, db_conn & cursor alive")


def buttonDialogClickedHandler():
    dialog.setWindowTitle("Buch hinzufügen")
    infolist = list()
    dialog.lineEditAutor.clear()
    dialog.lineEditTitel.clear()
    dialog.lineEditVerlag.clear()
    dialog.lineEditEjahr.clear()
    dialog.lineEditGenre.clear()
    dialog.lineEditSprache.clear()
    dialog.lineEditRegkoord.clear()
    dialog.lineEditAutor.setText('')
    if dialog.exec_() == widgets.QDialog.Accepted:
        mainwindow.resultlabel.setText("Accepted in Dialog(1)")
        print("\nEingegebene Daten:\n==================", dialog.lineEditAutor.text(), sep="\n")
        infolist.append(dialog.lineEditAutor.text())
        infolist.append(dialog.lineEditTitel.text())
        infolist.append(dialog.lineEditEjahr.text())
        infolist.append(dialog.lineEditRegkoord.text())
        try:
            cmdstr = "INSERT INTO Bücher(Autor, Titel, Ejahr, Regkoord) VALUES('" + infolist[0] + "', '" + infolist[1] + "', " + infolist[2] + ", '" + infolist[3] + "');"
            print(cmdstr)
            db_conn.execute(cmdstr)
            print("execute done")
            db_conn.commit()
            print("commit done")
        except sqlite3.OperationalError:
            print("Error in buttonDialogClickedHandler")
    else:
        mainwindow.resultlabel.setText("Rejected in Dialog(1)")


def buttonDBClickedHandler():
    printDB()


def buttonDialog2ClickedHandler():
    infolist = list()
    autor, titel, regkoord = "", "", ""
    dialog2.lineEditAutor.clear()
    dialog2.lineEditTitel.clear()
    dialog2.lineEditRegkoord.clear()
    if dialog2.exec_() == widgets.QDialog.Accepted:
        mainwindow.resultlabel.setText("Accepted in Dialog2")
        infolist.append(dialog2.lineEditAutor.text())
        infolist.append(dialog2.lineEditTitel.text())
        infolist.append(dialog2.lineEditRegkoord.text())
        try:
            cmdstr = "SELECT * FROM Bücher WHERE Autor LIKE '%" + infolist[0] + "%' AND Titel LIKE '%" + infolist[1] + "%' AND Regkoord LIKE '%" + infolist[2] + "%';"
            print(cmdstr)
            print("\n\n")
            result = db_conn.execute(cmdstr)
            for row in result:
                mainwindow.listWidget.addItem(str(row[1]) + ": »" + str(row[2]) + "« (" + str(row[3]) + ") @ " + str(row[4]))
                print("Successfully added row " + str(row))
            print("\n\n")
            print("execute done")
        except sqlite3.OperationalError:
            print("Error in buttonDialog2ClickedHandler")
        except:
            print("Unknown Error in buttonDialog2ClickedHandler")
        finally:
            dialog2.checkBoxAutor.setChecked(True)
            dialog2.checkBoxTitel.setChecked(True)
            dialog2.checkBoxRegkoord.setChecked(False)
            dialog2.label3.setEnabled(False)
            dialog2.lineEditRegkoord.setEnabled(False)
            print("checkbox unchecked, label3+RK-lineEdit disabled")
    else:
        mainwindow.resultlabel.setText("Rejected in Dialog2")
        dialog2.checkBoxAutor.setChecked(True)
        dialog2.checkBoxTitel.setChecked(True)
        dialog2.checkBoxRegkoord.setChecked(False)
        dialog2.label3.setEnabled(False)
        dialog2.lineEditRegkoord.setEnabled(False)
        print("checkbox unchecked, label3+RK-lineEdit disabled")


def printDB():
    try:
        result = db_conn.execute("SELECT * FROM Bücher;")
        for row in result:
            print("\nAutor: " + row[1])
            print("Titel: " + row[2])
            print("Ejahr: " + str(row[3]))
            print("Regkoord: " + row[4])
    except sqlite3.OperationalError:
        print("Error in printDB")
    except sqlite3.Error as e:
        print(e.__cause__)
    except:
        print("Unknown error in printDB")


def listWidgetItemClickedHandler():
    print('irgendwas selected')


def buttonChangeClickedHandler():
    dialog.setWindowTitle("Eintrag zu Buchdaten ändern")
    infolist = list()
    dialog.lineEditAutor.clear()
    dialog.lineEditTitel.clear()
    dialog.lineEditVerlag.clear()
    dialog.lineEditEjahr.clear()
    dialog.lineEditGenre.clear()
    dialog.lineEditSprache.clear()
    dialog.lineEditRegkoord.clear()
    dialog.lineEditAutor.setText('')

    try:
        infostr = mainwindow.listWidget.selectedItems()[0].text() # läuft!
        print(infostr)
        ### nimm den Autor und den Titel aus infostr, jag sie durch die DB und lade die Einzeldaten daraus in den geladenen Dialog!
        autor = infostr.split('»')[0].strip()[:-1]
        titel = infostr.split('»')[1].split('«')[0]
        cmdstr = "SELECT * FROM Bücher WHERE Autor='" + autor + "' AND Titel='" + titel + "';"
        print(cmdstr)
        result = db_conn.execute(cmdstr)
        for row in result:
            print(row)
            infolist.append(row)
        print(infolist)
        dialog.lineEditAutor.setText(infolist[0][1])
        dialog.lineEditTitel.setText(infolist[0][2])
        dialog.lineEditEjahr.setText(str(infolist[0][3])) # str im eigentl. Programm unnötig, cf. neues DB-Model ganz unten↓↓↓
        dialog.lineEditRegkoord.setText(infolist[0][4])
        if dialog.exec_() == widgets.QDialog.Accepted:
            mainwindow.resultlabel.setText("Accepted in Dialog(1) during UPDATE")
            cmdstr = "UPDATE Bücher SET Autor='" + dialog.lineEditAutor.text() + "', Titel='" + dialog.lineEditTitel.text() + "', Ejahr=" + dialog.lineEditEjahr.text() + ", Regkoord='" + dialog.lineEditRegkoord.text() + "' WHERE Autor='" + autor + "' AND Titel='" + titel + "';"
            print(cmdstr)
            db_conn.execute(cmdstr)
            print("^^^ executed!")
        else:
            mainwindow.resultlabel.setText("Rejected in Dialog(1) during UPDATE")
    except IndexError:
        print("No item selected, caused error in buttonChangeClickedHandler")
    except sqlite3.OperationalError:
        print("sqlite3 encountered an OperationalError for whatever reason in buttonChangeClickedHandler")
    finally:
        mainwindow.listWidget.clear()
        print("cleared out mainwindow.listWidget")


def buttonDeleteClickedHandler():
    try:
        infostr = mainwindow.listWidget.selectedItems()[0].text() # läuft!
        print(infostr)
        ### nimm den Autor und den Titel aus infostr, jag sie durch die DB und lade die Einzeldaten daraus in den geladenen Dialog!
        autor = infostr.split('»')[0].strip()[:-1]
        titel = infostr.split('»')[1].split('«')[0]
        cmdstr = "SELECT * FROM Bücher WHERE Autor='" + autor + "' AND Titel='" + titel + "';"
        print(cmdstr)
        result = db_conn.execute(cmdstr)
        for row in result:
            print(row)
            infolist.append(row)
        print(infolist, infolist[0], infolist[0][0], sep='\n')
        cmdstr = "DELETE FROM Bücher WHERE ID=" + str(infolist[0][0]) + ";"
        print(cmdstr)
        db_conn.execute(cmdstr)
        print("execution done, deleted entry", infolist, sep=' ')
        db_conn.commit()
        print("commit done")
    except IndexError:
        print("No item selected, caused error in buttonDeleteClickedHandler")
    except sqlite3.OperationalError:
        print("sqlite3 encountered an OperationalError for whatever reason in buttonDeleteClickedHandler")
    finally:
        mainwindow.listWidget.clear()
        print("cleared out mainwindow.listWidget")


def mpft():
    try:
        print(mainwindow.listWidget.selectedItems()[0].text())
    except IndexError:
        print("No item selected, caused eror in mpft")


def dialog2AutorEnDisableHandler():
    if not dialog2.lineEditAutor.isEnabled():
        dialog2.lineEditAutor.setEnabled(True)
    else:
        dialog2.lineEditAutor.setEnabled(False)
    print("lineEditAutor enabled/disabled")


def dialog2TitelEnDisableHandler():
    if not dialog2.lineEditTitel.isEnabled():
        dialog2.lineEditTitel.setEnabled(True)
    else:
        dialog2.lineEditTitel.setEnabled(False)
    print("lineEditTitel enabled/disabled")


def dialog2RKEnDisableHandler():
    if not dialog2.label3.isEnabled() and not dialog2.lineEditRegkoord.isEnabled():
        dialog2.label3.setEnabled(True)
        dialog2.lineEditRegkoord.setEnabled(True)
    else:
        dialog2.label3.setEnabled(False)
        dialog2.lineEditRegkoord.setEnabled(False)
    print("label3+lineEditRegkoord enabled/disabled")


def main():
    mainwindow.show()
    print("mainwindow shown")
    mainwindow.buttonDialog.clicked.connect(buttonDialogClickedHandler)
    mainwindow.buttonDialog2.clicked.connect(buttonDialog2ClickedHandler)
    mainwindow.buttonDB.clicked.connect(buttonDBClickedHandler)
    mainwindow.listWidget.itemClicked.connect(listWidgetItemClickedHandler)
    mainwindow.buttonChange.clicked.connect(buttonChangeClickedHandler)
    mainwindow.buttonDelete.clicked.connect(buttonDeleteClickedHandler)

    mainwindow.stubButton.clicked.connect(mpft)

    dialog2.checkBoxAutor.stateChanged.connect(dialog2AutorEnDisableHandler)
    dialog2.checkBoxTitel.stateChanged.connect(dialog2TitelEnDisableHandler)
    dialog2.checkBoxRegkoord.stateChanged.connect(dialog2RKEnDisableHandler)
    print("Verdrahtung passiert")

    try:
        db_conn.execute("CREATE TABLE Bücher(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Autor TEXT NOT NULL, Titel TEXT NOT NULL, Ejahr INTEGER, Regkoord TEXT NOT NULL);")
        print("table created")
        db_conn.execute("INSERT INTO Bücher(Autor, Titel, Ejahr, Regkoord) VALUES('Günter Grass', 'Schnäppchen DDR', 1991, '1 | 3');")
        print("Grass added")
    except sqlite3.OperationalError:
        print("DB already exists")

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

'''
richtiges DB-Model (Attribute):
CREATE TABLE Bücher(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Autor TEXT NOT NULL, Titel TEXT NOT NULL, Untertitel TEXT, Verlag TEXT, Ejahr TEXT NOT NULL, Genre TEXT, Sprache TEXT, Regkoord TEXT NOT NULL, Last_action DATE DEFAULT (datetime('now', 'localtime')));


Verbesserungsvorschlag: Ejahr als TEXT statt INTEGER, trotzdem NOT NULL

This file is derived from ~/sqlTEST/program-new.py
'''
